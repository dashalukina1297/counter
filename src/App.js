import "./App.css";
const INCREASE = "INCREASE";
const DECREASE = "DECREASE";

function App({ state, dispatch }) {
  return (
    <div className='block'>
      <div className='block__number'>{state.count}</div>
      <div className='block__buttons'>
        <button onClick={() => dispatch({ type: INCREASE })}>+</button>
        <button onClick={() => dispatch({ type: DECREASE })}>-</button>
      </div>
    </div>
  );
}

export default App;
